# tag 0.1
## Copyleft 2012, Carlos Zayas Guggiari <carlos@zayas.org>

### Uso:

    ./tag.py file|dir [file|dir] [...]

### Descripción:

Permite ajustar el nombre de los archivos MP3 con las etiquetas "Artista" y "Titulo", en base al nombre en formato "Artista - Titulo.mp3".

Si el nombre cumple ese formato, se pueden arreglar las etiquetas, o viceversa (Renombrar archivo en base a las etiquetas).
