#!/usr/bin/env python
#-*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Tag - Version 1.0 - 01/06/2012
# Carlos Zayas Guggiari <carlos@carloszayas.com>
# python --version : Python 2.7.3+
# -----------------------------------------------------------------------------

import os, sys

try:
    import eyeD3 # http://eyed3.nicfit.net/
except ImportError, e:
    print 'Error:',e,'<http://eyed3.nicfit.net>'
    print 'sudo apt-get install eyed3 python-eyed3'
    sys.exit()

reload(sys)

sys.setdefaultencoding('utf-8')

#------------------------------------------------------------------------------
# Variables globales
#------------------------------------------------------------------------------

todo      = False # Repetir opcion en todos los archivos
antes     = ''    # Opcion elegida anteriormente
ignorados = []    # Lista de archivos que no fueron renombrados

linea_simple = '-' * 80
linea_doble  = '=' * 80

#------------------------------------------------------------------------------
# Captura de teclas
#------------------------------------------------------------------------------

try:
    # DOS, Windows
    import msvcrt
    getkey = msvcrt.getch
except ImportError:
    # Se asume Unix
    import tty, termios

    def getkey():

        'Retorna una tecla presionada.'

        file = sys.stdin.fileno()
        mode = termios.tcgetattr(file)

        try:
            tty.setraw(file, termios.TCSANOW)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(file, termios.TCSANOW, mode)

        return ch

#------------------------------------------------------------------------------
# Funciones
#------------------------------------------------------------------------------

def args():

    'Retorna una lista con los argumentos de la línea de comandos.'

    lista = []

    if len(sys.argv) > 1:
        lista = sys.argv[1:]

    return lista

#------------------------------------------------------------------------------

def esmp3(archivo):

    'Retorna verdadero si el archivo es del tipo MP3.'

    return archivo.lower().endswith('.mp3') and eyeD3.isMp3File(archivo)

#------------------------------------------------------------------------------

def ficha(lugar,nombre,artista,titulo):

    'Imprime ficha del archivo MP3.'

    print 'Lugar   : ' + lugar
    print 'Nombre  : ' + nombre
    print linea_simple
    print 'Artista : ' + artista
    print 'Titulo  : ' + titulo
    print linea_doble

#------------------------------------------------------------------------------

def tags(archivo):

    'Retorna las etiquetas ID3 principales del archivo MP3.'

    lugar,nombre,artista,titulo = '','','',''

    if esmp3(archivo):
        mp3 = eyeD3.Tag()
        mp3.link(archivo)
        lugar,nombre = os.path.split(archivo)
        artista = mp3.getArtist()
        titulo = mp3.getTitle()

    return lugar,nombre,artista,titulo

#------------------------------------------------------------------------------

def tag(archivo):

    'Modifica el nombre o las etiquetas ID3 del archivo.'

    global todo, antes, ignorados

    menu = 'Arreglar: [N] Nombre [E] Etiquetas [S] Saltar [T] Todo [C] Cancelar'

    salir = False

    lugar,nombre,artista,titulo = tags(archivo)

    if lugar:

        ficha(lugar,nombre,artista,titulo)

        while not salir:

            opcion = antes

            if not todo:
                print menu
                print linea_doble
                opcion = getkey().upper()

            if opcion == 'N':
                antes = opcion
                nuevo = os.path.join(lugar,artista+' - '+titulo+'.mp3')
                if os.path.exists(nuevo):
                    ignorados.append(archivo)
                    print 'ATENCION: No se puede cambiar de nombre al archivo.'
                    print linea_doble
                else:
                    os.rename(archivo,nuevo)
                    print 'Antes: "%s"' % nombre
                    print 'Ahora: "%s - %s.mp3"' % (artista,titulo)
                    print linea_doble
            elif opcion == 'E':
                antes = opcion
                nuevo = nombre[:-4].split('-')
                artista = nuevo[0].strip()
                titulo = nuevo[1].strip()
                mp3 = eyeD3.Tag()
                mp3.link(archivo)
                mp3.setArtist(artista)
                mp3.setTitle(titulo)
                mp3.update()
                print 'Artista : "%s"' % artista
                print 'Titulo  : "%s"' % titulo
                print linea_doble
            elif opcion == 'S':
                pass
            elif opcion == 'T':
                todo = antes != ''
            elif opcion == 'C':
                sys.exit()
            else:
                print opcion+'?\033[3A' # Retrocede el cursor 3 lineas

            salir = opcion in 'NESC'

#------------------------------------------------------------------------------
# Bloque principal
#------------------------------------------------------------------------------

def main():

    'Funcion principal.'

    lista = args()

    print linea_doble
    print 'tag 0.1 - Copyleft 2012, Carlos Zayas Guggiari <carlos@zayas.org>'
    print linea_doble

    if lista:

        for arg in lista:

            item = os.path.abspath(arg)

            if os.path.exists(item):

                if os.path.isdir(item):
                    for raiz, carpetas, archivos in os.walk(item):
                        for archivo in archivos:
                            tag(os.path.join(raiz,archivo))

                if os.path.isfile(item):
                    tag(item)

        if ignorados:

            print 'Los siguientes archivos fueron ignorados:'
            print linea_simple
            for ignorado in ignorados: print ignorado
            print linea_doble

    else:

        print 'Uso     : %s file|dir [file|dir] [...]' % sys.argv[0]
        print 'Ayuda   : Permite ajustar el nombre de los archivos MP3 con las'
        print '          etiquetas "Artista" y "Titulo", en base al nombre en'
        print '          formato "Artista - Titulo.mp3". Si el nombre cumple'
        print '          ese formato, se pueden arreglar las etiquetas, o'
        print '          viceversa (Renombrar archivo en base a las etiquetas).'
        print linea_doble

#------------------------------------------------------------------------------
# Este archivo puede importarse como módulo gracias a la siguiente sentencia:
#------------------------------------------------------------------------------

if __name__ == '__main__': main()

#==============================================================================
